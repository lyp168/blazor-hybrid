﻿using BlazorHybrid.Shared;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace BlazorHybrid.Server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BlazorHybridModeController : ControllerBase
    {
        private readonly IMemoryCache _memoryCache;

        public BlazorHybridModeController(IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
        }


        [HttpGet("Set")]
        public async Task Set(string hybridMode)
        {
            await Task.Run(() =>
            {
                _memoryCache.Set("hybridMode", hybridMode);
            });
        }

        [HttpGet]
        public async Task<string> Get()
        {
            return await Task.Run(() =>
            {
                var result = _memoryCache.Get("hybridMode")?.ToString()!;
                return result ?? "HybridOnNavigation";
            });
           
        }
    }
}
